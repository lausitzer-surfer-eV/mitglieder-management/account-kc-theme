## Lausitzer Surfer e.V. base theme for Keycloak

![](demo.webp)

- Download `keycloak-24.0.2.tar.gz`
    - from: https://github.com/keycloak/keycloak/releases
    or clone from:
    https://github.com/keycloak/keycloak/tree/main/themes/src/main/resources/theme/base/login
- extract
- find `org.keycloak.keycloak-themes-24.0.2.jar` under:
    - `lib/lib/main/org.keycloak.keycloak-themes-24.0.2.jar`
- extract
    - find `account` and `login` under:
        - `theme\keycloak.v2`

## Docker Setup Tutorial (custom-auth-service)

The theme overrides the base path behavior based [on a Github discussion](https://github.com/keycloak/keycloak/discussions/10467#discussioncomment-6069298).

First, follow the basic setup described [here](https://saurav-samantray.medium.com/dockerize-keycloak-21-with-a-custom-theme-b6f2acad03d5) to create a custom theme locally for docker testing.

The Dockerfile:
```dockerfile
FROM quay.io/keycloak/keycloak:24.0.2 as builder

# Configure a database vendor
ENV KC_DB=postgres

WORKDIR /opt/keycloak

# for demonstration purposes only, please make sure to use proper certificates in production instead
RUN keytool -genkeypair -storepass password -storetype PKCS12 -keyalg RSA -keysize 2048 -dname "CN=server" -alias server -ext "SAN:c=DNS:localhost,IP:127.0.0.1" -keystore conf/server.keystore

RUN /opt/keycloak/bin/kc.sh build

FROM quay.io/keycloak/keycloak:24.0.2
COPY --from=builder /opt/keycloak/ /opt/keycloak/

COPY ./themes /opt/keycloak/themes

# Adding custom ENTRYPOINT
ENTRYPOINT ["/opt/keycloak/bin/kc.sh"]
```

Modify `theme/welcome/theme.properties`:
```
import=common/keycloak
locales=en
redirectToAdmin=false
```

Modify `theme/welcome/index.ftl`:
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="refresh" content="0; url=${baseUrl}realms/members/account/#/" />
    <meta name="robots" content="noindex, nofollow" />
    <script type="text/javascript">
      window.location.href = '${baseUrl}realms/members/account/#/'
    </script>
  </head>
  <body>
    If you are not redirected automatically, follow this <a href="${baseUrl}realms/members/account/#/">link</a>.
  </body>
</html>
```


Build the image with the theme and test locally:
```bash
docker build . -t mykeycloak
docker run --name mykeycloak -p 8443:8443 -p 8080:8080 \
        -e KEYCLOAK_ADMIN=admin -e KC_SPI_THEME_WELCOME_THEME=welcome_new -e KEYCLOAK_ADMIN_PASSWORD=test \
        mykeycloak \
        start-dev
```

or the alternative with `docker-compose.yml`:
```yaml
services:
  keycloak:
    build: .
    container_name: mykeycloak
    ## optionally bind folder, for live development
    ## note that cache is automatically disabled, if
    ## keycloak is started with start-dev
    # volumes:
    #   - ./themes:/opt/keycloak/themes
    environment:
      KEYCLOAK_ADMIN: admin
      KEYCLOAK_ADMIN_PASSWORD: test
      KC_SPI_THEME_WELCOME_THEME: nerdcore
    command: start-dev
    ports:
      - 8443:8443
      - 8080:8080
```

```bash
docker compose build && docker compose up
```

To redirect users directly to the login screen:

Instead of 
```bash
'${baseUrl}realms/members/account/#/'
```

use:
```bash
'${baseUrl}/realms/members/account/#/personal-info'
```

If the user is already logged in, he/she will get forwarded directly to account management. Otherwise, a token will be generated and forwarded to the login screen directly.