<#macro content>
    <!-- ToDo: Update when shipped https://github.com/keycloak/keycloak/issues/31390 -->
    <!-- hello from footer.ftl -->
    <div>
        <#-- footer at the end of the login box -->
            <!--<hr id="kc-login-footer-divider"> -->
            <ul id="kc-login-footer-links">
                <li><a href="https://lausitzer-surfer.de/">CC BY-NC-SA 4.0 2024 Lausitzer Surfer (e.V.)</a>, </li>
                <li><a href="https://lausitzer-surfer.de/impressum.html">Impressum</a>, </li>
                <li><a href="https://lausitzer-surfer.de/pdf/04_Datenschutzerkl%C3%A4rung_Mitglieder.pdf">Datenschutz</a></li>
            </ul>
            <div>
</#macro>