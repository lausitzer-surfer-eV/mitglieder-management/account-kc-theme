
(function (window, document, undefined) {

    // code that should be taken care of right away

    window.onload = init;

    function init() {
        // the code to be called when the dom has loaded
        var eterms = document.getElementById('terms')
        if (eterms) {
            var aterms = eterms.getElementsByTagName('a');
            if (aterms) {
                for (var i = 0; i < aterms.length; i++) {
                    aterms[i].setAttribute("target", "_blank");
                }
            }
        }
    }

})(window, document, undefined);

