<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="refresh" content="0; url=${baseUrl}realms/${properties.realmForwardTo}/account/#/" />
    <meta name="robots" content="noindex, nofollow" />
    <script type="text/javascript">
      window.location.href = '${baseUrl}realms/${properties.realmForwardTo}/account/#/'
    </script>
  </head>
  <body>
    If you are not redirected automatically, follow this <a href="${baseUrl}realms/${properties.realmForwardTo}/account/#/">link</a>.
  </body>
</html>